const child_process = require('child_process');
const fetch = require('sync-fetch');
const fs = require('fs-extra');
const path = require('path');
const { unzip } = require('cross-unzip');
const { getGamePath } = require('steam-game-path');

const GAMEID = 887570;

(async () => {
  //Clear gamedir directory
  console.info(`[Docker Setup] Clearing ./tmp/gamedir`);
  try { fs.rmSync(path.resolve('./tmp/gamedir'), { recursive: true, force: true }); } catch(err) {}
  try { fs.mkdirSync(path.resolve('./tmp/gamedir')); } catch(err) {}
  
  //Get steam game path
  console.info(`[Docker Setup] Searching for game in Steam`);
  let gamePathObject = getGamePath(GAMEID, true);
  let gamePathString = path.resolve(gamePathObject.game.path);

  //Copy steam game into gamedir
  console.info(`[Docker Setup] Copying Steam game files into ./tmp/gamedir`);
  try{ fs.copySync(gamePathString, path.resolve('./tmp/gamedir')); } catch(err) {}

  //Build mod
  console.info(`[Docker Setup] Building container`);
  let buildProcess = child_process.spawnSync('docker.exe', ['build', '.', '-f', './src/Dockerfile', '-t', 'nfc-dll:latest']);
  console.info(buildProcess.stdout.toString());
  console.info(buildProcess.stderr.toString());
})();
