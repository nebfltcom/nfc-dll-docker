# NFC DLL Docker

Docker image for building NFC mods

Uses Node.js to automatically setup and build docker.

# Usage

## Prerequisite

1) .NET SDK 6.0.200
    - https://dotnet.microsoft.com/en-us/download/visual-studio-sdks.
2) Node.js v16.13.2 and NPM 8.1.2
    - https://nodejs.org/en/download/
3) `node-gyp` prerequites (https://github.com/nodejs/node-gyp#on-windows)

*Note: Exact versions are not strictly required and older versions may work. These version numbers are provided for replicating known working setup.*

## Installation

1) Git clone or download this repository (https://gitlab.com/nebfltcom/nfc-dll-docker/-/archive/main/nfc-dll-docker.zip). Unzip if needed.
2) Open up a command prompt or powershell session and navigate to the folder that contains this file.
3) Run the command `npm install`.
4) Run the command `npm run setup`.
